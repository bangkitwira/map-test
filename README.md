# Mile Test

This repository created to answer the Front End Developer tests as shown below :

## Answer number 1
I prefer vue js other then react because the first thing the syntax is more simple, let say for template we can use HTML or JSX. Then for loops and conditions vue provide directives like v-for v-if and other things. Handling forms vue have v-model to bind directly to input value. For components props vue can define and validate the props. For caching vue have computed properties for caching data and v-once for template caching. For manipulating DOM vue have custom directives and refs. For conditional class attribute we can use :class or :style. Thats why I prefer to use vue because it will speed up development process for me because everything is already served.

## Answer number 2
When I develop front end mobile apps using ionic, at that moment need to have bottom sheet navigation's with swipe gesture. Because ionic doesnt have that component, so I need to create the own components using gesture swipe up and down to show and hide the bottom navigations.

## Answer number 3
Because I think UI developer also need to know whether the UI that been developed or created will be satisfying the end user and the end user is easy to understand how to use and what's the meaning of that designs.

## Answer number 4
For the UI side I think the copyright and the form should have more space, and the login button maybe would be good if it take up tha remain space or full width. For the header should have more space so it doesn't look's good also with the font. The organization name I think is confusing for the user prespective, because we don't even know whether it for the new organization or existing organization. Mile app logo maybe will be batter if placed on the left side of the header.

## Answer number 5a & 5b
For answer number 5 you can `clone` this repository and run `npm install` on the root of project directory and run `npm run dev` to serve on your local machine or you can visit this url [deployed netlify](https://6305c92c31d1ae42c37214e5--zippy-fox-50f9e8.netlify.app/).

## Answer number 6a

```sh
let a = 3, b = 5;
[a, b] = [b, a];
console.log(a, b);
```

### Answer number 6b

```sh
let numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100],
    count = 100,
    missing = []

for (let i = 1; i <= count; i++) {
    if (numbers.indexOf(i) === -1) {
        missing.push(i)
    }
}
console.log(missing)

```

### Answer number 6c

```sh
const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];
const duplicates = numbers.filter((item, index) => index !== numbers.indexOf(item));

console.log(Array.from(new Set(duplicates)))

```

### Answer number 6d

```sh
const array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."]

const transformDec = array_code.reduce(
    (previousValue, currentValue, currentIdx, array) => {
        const findOneDecimal = parseInt(array.filter(arr => arr.length === 2))
        const findTwoDecimal = array.filter(arr => arr.length === 4).map(num => (num.charAt(2)))
        const findThreeDecimal = array.filter(arr => arr.length === 6)
        const findDec = findTwoDecimal.map((num, idx) => {
            const filter = findThreeDecimal.filter(dec => dec.charAt(2) === num)
            return { [num]: filter.reduce((a, v) => ({ ...a, [v.charAt(4)]: v }), {}) }
        })
        const convertObj = Object.assign({}, findDec)
        const combine = { [findOneDecimal]: convertObj }
        return combine
    }
);

console.log(transformDec)

```
