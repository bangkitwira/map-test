export default () => {
  if (localStorage.getItem("isAuthenticated")) return true;

  return false;
};
