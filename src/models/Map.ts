export interface Map {
  position: {
    lat: number
    lng: number
  }
  description?: string
}
