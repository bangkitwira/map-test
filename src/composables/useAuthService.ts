import { useRouter } from "vue-router";
import type { User } from "@/models/User";

const useAuthService = () => {
  const router = useRouter();

  const checkUser = (payload: User) => {
    if (payload.username === "admin" && payload.password === "mileapp") {
      localStorage.setItem(
        "isAuthenticated",
        JSON.stringify({ authenticated: true })
      );
      router.push({ name: "home" });
    }
  };

  return { checkUser };
};

export default useAuthService;
